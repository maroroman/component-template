// app headers
#include "cli/client.h"

// standard libraries
#include <string.h>
#include <stdio.h>

// SALLY libraries
#include <service/service.h>
#include <service/service_filesystem.h>
#include <service/service_com.h>

// headers shared for both client and server
#include "packets.h"

// SALLY backends provided by the library user
static service_fs_t* client_fs = NULL;
static service_os_t* client_os = NULL;
static service_com_t* client_com = NULL;

int16_t client_ping(service_com_handle_t hcom, char* rpl) {
  // send a ping
  client_cmd_send(hcom, DRIVER_TEMPLATE_CMD_PING, NULL, 0);

  // get the reply
  size_t ret = client_rpl_get(hcom, (uint8_t*)rpl, strlen(DRIVER_TEMPLATE_CMD_PING_RPL));
  if(ret != strlen(DRIVER_TEMPLATE_CMD_PING_RPL)) {
    return(-1);
  }

  return(0);
}

int16_t client_echo(service_com_handle_t hcom, char* out, char* in) {
  // send the echo request
  size_t echo_len = strlen(in);
  client_cmd_send(hcom, DRIVER_TEMPLATE_CMD_ECHO, (uint8_t*)in, echo_len);

  // get the reply
  size_t ret = client_rpl_get(hcom, (uint8_t*)out, echo_len);
  if(ret != echo_len) {
    return(-1);
  }

  out[ret] = '\0';
  return(0);
}

int16_t client_kill_server(service_com_handle_t hcom) {
  // send the kill request
  client_cmd_send(hcom, DRIVER_TEMPLATE_CMD_KILL, NULL, 0);
  size_t ret = client_rpl_get(hcom, NULL, 0);

  // there should be no reply to this
  if(ret != 0) {
    return(-1);
  }

  return(0);
}

// the following command/reply functions are for illustration only!
void client_cmd_send(service_com_handle_t hcom, uint8_t cmd_id, uint8_t* data, uint8_t data_len) {
  uint8_t buff[64] = { 0 };
  buff[0] = cmd_id;

  if((data != NULL) && (data_len > 0)) {
    memcpy(&buff[1], data, data_len);
  }

  memcpy(&buff[1 + data_len], DRIVER_TEMPLATE_CMD_LINE_END, strlen(DRIVER_TEMPLATE_CMD_LINE_END));
  client_com->write(hcom, buff, 1 + data_len + strlen(DRIVER_TEMPLATE_CMD_LINE_END));
}

size_t client_rpl_get(service_com_handle_t hcom, uint8_t* buff, size_t target_len) {
  size_t len = 0;
  while(len < target_len) {
    size_t res = client_com->read(hcom, buff + len, target_len);
    if(res >= 0) {
      len += res;
    } else {
      return(res);
    }
  }
  return(len);
}

void client_set_backends(service_fs_t* fs, service_os_t* os, service_com_t* com) {
  client_fs = fs;
  client_os = os;
  client_com = com;
}
